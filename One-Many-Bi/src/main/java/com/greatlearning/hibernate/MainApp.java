package com.greatlearning.hibernate;

import com.greatlearning.hibernate.dao.CourseDao;
import com.greatlearning.hibernate.dao.InstructorDao;
import com.greatlearning.hibernate.entity.Course;
import com.greatlearning.hibernate.entity.Instructor;

public class MainApp {
    public static void main(String[] args) {
        InstructorDao instructorDao = new InstructorDao();
        CourseDao courseDao = new CourseDao();

        Instructor instructor = new Instructor("Krishna", "Gupta", "krishna@gmail.com");
        instructorDao.saveInstructor(instructor);

        // create some courses
        Course tempCourse1 = new Course("Air Guitar - The Ultimate Guide");
        tempCourse1.setInstructor(instructor);
        courseDao.saveCourse(tempCourse1);

        Course tempCourse2 = new Course("The Pinball Masterclass");
        tempCourse2.setInstructor(instructor);
        courseDao.saveCourse(tempCourse2);
    }
}
